require('dotenv').config();
const fetch = require('cross-fetch');
const express = require('express');
const fs = require('fs');

const app = express();
const needle =
  process.env.APP_NEEDLE_CLIENTS_NAME?.split(',').map((item) => item.trim()) ||
  [];

const URI = process.env.APP_URL?.trim() || '/';
const PATH = process.env.APP_LOGFILE_PATH?.trim() || './log.log';
const CHAT_ID = process.env.TELEGRAM_CHAT_ID?.trim() || '';
const BOT_API = process.env.TELEGRAM_BOT_API_KEY?.trim() || '';
const WEBSERVER =
  process.env.APP_WEBSERVER?.trim().toLowerCase() === 'false' ||
  process.env.APP_WEBSERVER?.trim().toLowerCase() === '0'
    ? false
    : true;

const clientsArr = [];
let messageArr = [];
const PORT = process.env.APP_PORT?.trim() || '3001';
const TIMEOUT = +process.env.APP_WAITING_TIMEOUT_IN_MS?.trim() || 7000;

const telegramBotApi = `https://api.telegram.org/bot${BOT_API}/sendMessage?chat_id=${CHAT_ID}`;

async function sendMessage(url = '', message = '') {
  const encoded = encodeURI(url);
  try {
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        text: message,
        entities: [{ offset: 0, length: message.length, type: 'code' }],
      }),
    });
    const resp = await response.json();
    // console.log(resp);
    return resp;
  } catch (error) {
    console.log('Ошибка');
  }
}

const general = {
  start: null,
};
general.start = new Date();
const file = {
  readable: true,
};
class Client {
  constructor(client) {
    this.client = client;
    this.lastseen = null;
    this.laststate = true;
    this.nowstate = false;
  }
  timeout() {
    return (new Date() - this.lastseen) / 1000;
  }
}

needle.forEach((i) => clientsArr.push(new Client(i)));

let interval = new setInterval(() => {
  try {
    const data = fs.readFileSync(PATH, 'utf8');

    if (file.readable === false) {
      messageArr.push(`🚀 Файл ${PATH} доступен для чтения с ${new Date()}`);

      file.readable = true;
    }

    clientsArr.forEach((client) => {
      if (data.includes(client.client)) {
        client.lastseen = new Date();
        client.laststate = client.nowstate;
        client.nowstate = true;
      } else {
        client.laststate = client.nowstate;
        client.nowstate = false;
      }
    });
  } catch (err) {
    if (file.readable === true) {
      messageArr.push(`⚠ Не могу прочитать файл ${PATH} c ${new Date()}`);
      file.readable = false;
    }
  }

  clientsArr.forEach((item) => {
    const { client, lastseen, laststate, nowstate } = item;
    if (laststate === !nowstate && file.readable) {
      if (nowstate) {
        const message = `🚀 Связь с ${client} установлена в ${lastseen}`;
        messageArr.push(message);
      } else {
        const message = `⚠ Связь с ${client} потеряна в ${lastseen}`;
        messageArr.push(message);
      }
    }
  });
}, TIMEOUT);

const sendMessageInterval = new setInterval(async () => {
  let tempArr = [];
  if (messageArr.length > 0) {
    for (let item = 0; item < messageArr.length; item++) {
      const message = messageArr[item];
      const data = await sendMessage(telegramBotApi, message);
      if (!data?.ok) {
        tempArr.push(message);
      }
    }
    messageArr = tempArr;
  }
}, 5000);
if (WEBSERVER) {
  app.get(URI, (req, res) => {
    let resp = `🚀 Сервер запущен ${general.start}\n`;
    clientsArr.forEach((client) => {
      resp += `${client.client} ${
        client.nowstate
          ? 'доступен 🚀'
          : 'недоступен в течение ' +
            client.timeout() +
            ' сек, с ' +
            client.lastseen +
            '⚠'
      }\n`;
    });
    res.set({ 'content-type': 'application/json; charset=utf-8' });
    res.status(200).end(resp);
  });

  app.listen(PORT, () =>
    console.log(`🚀 Сервер запущен на порту ${PORT} в ${general.start}`),
  );
}
