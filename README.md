## Скрипт проверки наличия подстроки в файле. С оповещением в Телеграмм

Установить git

```sh
sudo apt update
sudo apt upgrade
sudo apt install git
```

Установить node.js. Через установку [nvm](https://github.com/nvm-sh/nvm)

```sh
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash

nvm install node

nvm use node

```

Установить yarn

```sh
npm install --global yarn
```

Установить [pm2](https://pm2.keymetrics.io/docs/usage/quick-start/)

```sh
yarn global add pm2
```

Клонировать репозиторий

```sh
git clone https://gitlab.com/avathar/vu_watchdog.git
```

Перейти в папку с проектом

```sh
cd vu_watchdog
```

Установить зависимости

```sh
yarn install
```

Создать файл .env с настройками окружения

```sh
nano .env
```

Запустить сервер средствами pm2

```sh
pm2 start index.js --name vu_watchdog --max-memory-restart 300M  --log ../pm2.log
```

Можно посмотреть запущеный сервис

```sh
pm2 ls
```

Добавить в автозапуск (pm2 подскажет что нужно сделать)

```sh
pm2 startup

```

Сохранить набор запущеных сервисов

```sh
pm2 save

```

Для дальнейших перезапусков нужно посмотреть, например, id процесса, и перезапустить командой

```sh
pm2 stop 1
pm2 start 1
```

Например, получить обновления и перезапустить сервис

```sh
cd vu_watchdog
pm2 stop 1
git pull
pm2 start 1
```

## Параметры .env

| Параметр                  | Значение по умолчанию | Описание                                                   |
| ------------------------- | --------------------- | ---------------------------------------------------------- |
| APP_PORT                  | 3001                  | Порт на котором отображается страница с текущим состоянием |
| APP_URL                   | /                     | адрес страницы (например /info)                            |
| APP_WAITING_TIMEOUT_IN_MS | 7000                  | Таймаут перечитывания файла (в милисекундах)               |
| APP_LOGFILE_PATH          | ./log.log             | Путь к файлу                                               |
| APP_NEEDLE_CLIENTS_NAME   |                       | Подстроки для поиска в файле (через запятую)               |
| TELEGRAM_CHAT_ID          |                       | ID чата для телеграм-бота                                  |
| TELEGRAM_BOT_API_KEY      |                       | Ключ api телеграм-бота                                     |
| APP_WEBSERVER             | true                  | Нужно ли запускать вебсервер (для отключения FALSE или 0)  |
